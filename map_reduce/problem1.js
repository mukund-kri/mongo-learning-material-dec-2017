var mapper = function() {
  emit(this.season, 1)
}

var reducer = function(year, matches) {
  return Array.sum(matches)
}

db.matches.mapReduce(
  mapper,
  reducer,
  {out: "problem1_solution"}
)