# MongoDB MapReduce - Basics

This folder contains code that solves the data exercise defined below with
mongodb's map-reduce framework.

## Before you start

Follow the instructions given in the datasets/ipl folder load the IPL dataset
into monogo.

## The problem

In this data assignment you will transform raw data from IPL into graphs that
will convey some meaning / analysis. Generate the following graphs -

1. Plot the number of matches played per year of all the years in IPL
2. Plot a stacked bar chart of matches won of all teams over all the years of IPL
3. For the year 2016 plot the extra runs conceded per team
4. For the year 2015 plot the top economical bowlers

NOTE: For this (mongodb) exercise we will not plot any data. Only generate the
the data that answers these questions.
