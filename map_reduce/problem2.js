var mapper = function() {
  key = {team: this.season, winner: this.winner}
  emit(key, 1)
}

var reducer = function(key, winners) {
  return Array.sum(winners)
}

db.matches.mapReduce(
  mapper,
  reducer,
  {out: "problem2_solution"}
)
