# Mongo Learning material

Study material (mostly code) on mongo / node+mongo generated during the
MountBlue, November Cohort.

## Running

Most of the following exercises have fully runnable code, and in most cases
have data provided as CSV. These CSV files can loaded into mongo using the
`mongoimport` tool. Instuctions for each example is given in the individual
folders.

## Content

### Aggregation Framework
Solutions to the **Data Exercise 1 (IPL Data set)** implemented in MongoDB's
Aggregation Framework.

### Map Reduce
Partial Solution to the **Data Exercise 1 (IPL Data set)** implemented in
MongoDB's Map Reduce Framework.

### 23.12.2017 livesession
Code the class generated during the intreactive live session on **Aggregation
and Map Reduce** in mongodb.
