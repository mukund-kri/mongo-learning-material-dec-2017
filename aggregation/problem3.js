// Note: this versions is very suboptimal and runs very slowly. It's
// only purpose is as a demo for mongo aggreation.

db.deliveries.aggregate([
  {
    $match: {
      "extra_runs": { $gt: 0 }
    }
  },
  {
    $lookup: {
      from: 'matches',
      localField: "match_id",
      foreignField: "id",
      as: 'match'
    }
  },
  {
    $unwind: "$match"
  },
  {
    $match: {
      "match.season" : 2016
    }
  },
  {
    $group: {
      _id: "$bowling_team",
      extra_runs: { $sum: "$extra_runs" }
    }
  }
]);
