db.matchs.aggregate([
  {
    $group: {
      _id: "$season",
      matches: { $sum: 1}
    }
  }
])
