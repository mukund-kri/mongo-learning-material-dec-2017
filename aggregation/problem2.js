db.matches.aggregate([
  {
    $group: {
      _id: { year: "$season", team: "$team1"},
      numMatches: { $sum: 1 }
    }
  }
])
