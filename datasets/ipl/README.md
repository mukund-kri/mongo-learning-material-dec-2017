# IPL data set

This is a copy of the data set avalable at

https://www.kaggle.com/manasgarg/ipl

For more details on the structure of the data please visit the link given above.

## Loading

Use the following commands to load `matches.csv` into the collection `matches`
and `deleveries.csv` into collection `deleveries`. **Note:** these commands use
the `dataexercise` database.

```
$> mongoimport --db dataexercise --type csv --file matches.csv --collection matches --headerline

$> mongoimport --db dataexercise --type csv --file deleveries.csv --collection deleveries --headerline
```
