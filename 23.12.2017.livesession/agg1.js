db.matches.aggregate([
  { $match: { season: 2008 } },
  { $group: { _id: "$city", matches: { $push: {side1: "$team1", side2: "$team2" }}} }
]);
