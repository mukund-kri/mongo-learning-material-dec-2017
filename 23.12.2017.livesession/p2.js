db.matches.aggregate([
  { $group: {
    _id: {
      year: "$season",
      team: "$winner"
    },
    matchesWon: { $sum: 1 }
  }}
])
