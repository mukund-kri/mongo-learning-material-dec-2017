db.matches.aggregate([

  {
    $group: {
      _id: "$season",
      numMatches: { $sum: 1 }
    }
  },

  {
    $count: "total_seasons"
  }

]);
