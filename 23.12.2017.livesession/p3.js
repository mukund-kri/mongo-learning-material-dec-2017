db.deliveries.aggregate ([

  {
    $lookup: {
      from: 'matches',
      localField: 'match_id',
      foreignField: 'id',
      as: 'match'
    }
  },
  {
    $match: {
      "match.0.season": 2016
    }
  },
  {
    $group: {
      _id: "$bowling_team",
      extraRuns: { $sum: "$extra_runs"}
    }
  }
]).pretty()
